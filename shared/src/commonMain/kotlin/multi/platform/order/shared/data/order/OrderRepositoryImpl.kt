package multi.platform.order.shared.data.order

import io.ktor.client.call.body
import io.ktor.client.request.bearerAuth
import io.ktor.client.request.get
import multi.platform.core.shared.data.common.network.response.ApiResponse
import multi.platform.core.shared.external.utility.ApiClient
import multi.platform.order.shared.data.order.response.GetOrderFilterParametersResp
import multi.platform.order.shared.data.order.response.GetOrdersResp
import multi.platform.order.shared.domain.order.OrderRepository
import multi.platform.order.shared.domain.order.entity.Order

class OrderRepositoryImpl(
    private val apiClient: ApiClient
) : OrderRepository {

    override suspend fun getOrders(
        accessToken: String?,
        start: Int,
        limit: Int,
        status: String,
        dateRange: String,
    ): ApiResponse<GetOrdersResp?>? =
        apiClient.client.get("/api/trade/v1/transactions?start=$start&limit=$limit&status=$status&date_range=$dateRange") {
            accessToken?.let { bearerAuth(it) }
        }.body()

    override suspend fun getOrder(accessToken: String?, id: Int): ApiResponse<Order?>? =
        apiClient.client.get("/api/trade/v1/transactions/$id") {
            accessToken?.let { bearerAuth(it) }
        }.body()

    override suspend fun getOrderFilterParameters(accessToken: String?): ApiResponse<GetOrderFilterParametersResp?>? =
        apiClient.client.get("/api/trade/v1/parameters/transaction") {
            accessToken?.let { bearerAuth(it) }
        }.body()
}