package multi.platform.order.shared.app.order

import io.ktor.client.call.body
import io.ktor.client.plugins.ClientRequestException
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import multi.platform.core.shared.app.common.BaseViewModel
import multi.platform.core.shared.domain.common.entity.Meta
import multi.platform.core.shared.external.constant.AppConstant
import multi.platform.order.shared.domain.order.entity.Order
import multi.platform.order.shared.domain.order.entity.OrderFilter
import multi.platform.order.shared.domain.order.usecase.GetOrdersUseCase

@Suppress("kotlin:S6305")
class OrderViewModel(
    private val getOrdersUseCase: GetOrdersUseCase,
) : BaseViewModel() {
    var accessToken: String? = null
    val orders = MutableStateFlow<MutableList<Order>?>(null)
    val isEmpty = MutableStateFlow(false)
    val orderFilter = MutableStateFlow(OrderFilter())

    fun getOrders(page: Int) {
        scope.launch {
            loadingIndicator.value = true
            try {
                val response = getOrdersUseCase(
                    accessToken,
                    page,
                    AppConstant.LIST_LIMIT,
                    orderFilter.value.status.toString(),
                    orderFilter.value.date.toString()
                )
                isEmpty.value = false
                loadingIndicator.value = false
                orders.value = response?.data?.content
            } catch (e: Exception) {
                e.printStackTrace()
                if (e is ClientRequestException && e.response.status.value in arrayOf(
                        400,
                        401,
                        403
                    )
                ) {
                    val meta: Meta? = e.response.body()
                    meta?.let { errorMessage.value = meta.message }
                } else errorMessage.value = e.message.toString()
                isEmpty.value = true
                loadingIndicator.value = false
            }
        }
    }
}