package multi.platform.order.shared.app.orderdetail

import io.ktor.client.call.body
import io.ktor.client.plugins.ClientRequestException
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import multi.platform.core.shared.app.common.BaseViewModel
import multi.platform.core.shared.domain.common.entity.Meta
import multi.platform.order.shared.domain.order.entity.Order
import multi.platform.order.shared.domain.order.usecase.GetOrderUseCase

@Suppress("kotlin:S6305")
class OrderDetailViewModel(
    private val getOrderUseCase: GetOrderUseCase
) : BaseViewModel() {
    var accessToken: String? = null
    val order = MutableStateFlow<Order?>(null)

    fun getOrder(id: Int) {
        scope.launch {
            loadingIndicator.value = true
            try {
                val response = getOrderUseCase(accessToken, id)
                loadingIndicator.value = false
                order.value = response?.data
            } catch (e: Exception) {
                e.printStackTrace()
                if (e is ClientRequestException && e.response.status.value in arrayOf(
                        400,
                        401,
                        403
                    )
                ) {
                    val meta: Meta? = e.response.body()
                    meta?.let { errorMessage.value = meta.message }
                } else errorMessage.value = e.message.toString()
                loadingIndicator.value = false
            }
        }
    }
}