package multi.platform.order.shared.domain.order.usecase

import multi.platform.order.shared.domain.order.OrderRepository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class GetOrderUseCase : KoinComponent {
    private val orderRepository: OrderRepository by inject()
    suspend operator fun invoke(accessToken: String?, id: Int) =
        orderRepository.getOrder(accessToken, id)
}