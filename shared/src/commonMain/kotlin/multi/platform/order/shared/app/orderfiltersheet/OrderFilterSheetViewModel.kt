package multi.platform.order.shared.app.orderfiltersheet

import io.ktor.client.call.body
import io.ktor.client.plugins.ClientRequestException
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import multi.platform.core.shared.app.common.BaseViewModel
import multi.platform.core.shared.domain.common.entity.Meta
import multi.platform.order.shared.domain.order.entity.Label
import multi.platform.order.shared.domain.order.usecase.GetOrderFilterParametersUseCase

@Suppress("kotlin:S6305")
class OrderFilterSheetViewModel(
    private val getOrderFilterParametersUseCase: GetOrderFilterParametersUseCase,
) : BaseViewModel() {
    val statuses = MutableStateFlow<MutableList<Label>?>(null)
    val dates = MutableStateFlow<MutableList<Label>?>(null)
    val status = MutableStateFlow<String?>(null)
    val date = MutableStateFlow<String?>(null)

    fun getParameters(accessToken: String?) {
        scope.launch {
            loadingIndicator.value = true
            try {
                val response = getOrderFilterParametersUseCase(accessToken)
                loadingIndicator.value = false
                statuses.value = response?.data?.statuses
                dates.value = response?.data?.dates
            } catch (e: Exception) {
                e.printStackTrace()
                if (e is ClientRequestException && e.response.status.value in arrayOf(
                        400,
                        401,
                        403
                    )
                ) {
                    val meta: Meta? = e.response.body()
                    meta?.let { errorMessage.value = meta.message }
                } else errorMessage.value = e.message.toString()
                loadingIndicator.value = false
            }
        }
    }
}