package multi.platform.order.shared.domain.order.usecase

import multi.platform.order.shared.domain.order.OrderRepository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class GetOrdersUseCase : KoinComponent {
    private val orderRepository: OrderRepository by inject()
    suspend operator fun invoke(
        accessToken: String?,
        start: Int,
        limit: Int,
        status: String,
        dateRange: String,
    ) = orderRepository.getOrders(accessToken, start, limit, status, dateRange)
}