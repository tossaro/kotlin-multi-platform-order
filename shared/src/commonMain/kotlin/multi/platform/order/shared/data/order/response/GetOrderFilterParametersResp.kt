package multi.platform.order.shared.data.order.response

import kotlinx.serialization.Serializable
import multi.platform.order.shared.domain.order.entity.Label

@Serializable
data class GetOrderFilterParametersResp(
    val statuses: MutableList<Label>? = null,
    val dates: MutableList<Label>? = null,
)