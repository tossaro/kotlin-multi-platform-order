package multi.platform.order.shared.domain.order.entity

import kotlinx.serialization.Serializable

@Serializable
data class Label(
    val label: String? = null,
    val value: String? = null,
)