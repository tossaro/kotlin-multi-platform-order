package multi.platform.order.shared.domain.order.entity

import multi.platform.core.shared.Parcelable
import multi.platform.core.shared.Parcelize

@Parcelize
data class OrderFilter(
    var status: String? = "",
    var date: String? = "1",
) : Parcelable