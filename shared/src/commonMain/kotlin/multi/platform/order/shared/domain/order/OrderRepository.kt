package multi.platform.order.shared.domain.order

import multi.platform.core.shared.data.common.network.response.ApiResponse
import multi.platform.order.shared.data.order.response.GetOrderFilterParametersResp
import multi.platform.order.shared.data.order.response.GetOrdersResp
import multi.platform.order.shared.domain.order.entity.Order

interface OrderRepository {
    suspend fun getOrders(
        accessToken: String?,
        start: Int,
        limit: Int,
        status: String,
        dateRange: String,
    ): ApiResponse<GetOrdersResp?>?

    suspend fun getOrder(accessToken: String?, id: Int): ApiResponse<Order?>?

    suspend fun getOrderFilterParameters(accessToken: String?): ApiResponse<GetOrderFilterParametersResp?>?
}