package multi.platform.order.shared.domain.order.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import multi.platform.core.shared.external.constant.AppConstant

@Serializable
data class Order(
    val id: Int = 0,
    val title: String = AppConstant.LOADING,
    @SerialName("booked_date") val date: String = AppConstant.LOADING,
    @SerialName("order_no") val number: String = AppConstant.LOADING,
    @SerialName("sub_total") val price: String = AppConstant.LOADING,
    val discount: String = AppConstant.LOADING,
    val promo: String = AppConstant.LOADING,
    @SerialName("promo_price") val promoPrice: String = AppConstant.LOADING,
    @SerialName("base_price") val basePrice: String = AppConstant.LOADING,
    @SerialName("status_code") val statusCode: String = AppConstant.LOADING,
    val status: String = AppConstant.LOADING,
    @SerialName("package_id") val packageId: String = AppConstant.LOADING,
    val thumbnail: String? = null,
    val quota: String? = null,
    @SerialName("created_date") val createdDate: String? = null,
)