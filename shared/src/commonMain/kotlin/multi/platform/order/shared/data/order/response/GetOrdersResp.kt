package multi.platform.order.shared.data.order.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import multi.platform.order.shared.domain.order.entity.Order

@Serializable
data class GetOrdersResp(
    @SerialName("total_pages") val totalPages: Int? = null,
    @SerialName("total_elements") val totalElements: Int? = null,
    val last: String? = null,
    val size: Int? = null,
    val content: MutableList<Order>? = null,
)