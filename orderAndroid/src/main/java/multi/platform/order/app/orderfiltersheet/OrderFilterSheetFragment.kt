package multi.platform.order.app.orderfiltersheet

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.gson.Gson
import multi.platform.core.shared.app.common.BaseSheetFragment
import multi.platform.core.shared.external.constant.AppConstant
import multi.platform.order.R
import multi.platform.order.databinding.OrderFilterSheetFragmentBinding
import multi.platform.order.external.constant.OrderConstant
import multi.platform.order.shared.app.orderfiltersheet.OrderFilterSheetViewModel
import multi.platform.order.shared.domain.order.entity.Label
import multi.platform.order.shared.domain.order.entity.OrderFilter
import org.koin.androidx.viewmodel.ext.android.viewModel

class OrderFilterSheetFragment :
    BaseSheetFragment<OrderFilterSheetFragmentBinding>(R.layout.order_filter_sheet_fragment) {
    private val vm: OrderFilterSheetViewModel by viewModel()
    lateinit var orderFilter: OrderFilter
    override fun title() = getString(R.string.filter_title)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val filterStr = sharedPreferences.getString(OrderConstant.FILTER_BUNDLE, null)
        filterStr?.let {
            orderFilter = Gson().fromJson(it, OrderFilter::class.java)
        } ?: run {
            orderFilter = OrderFilter()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.vm = vm.also {
            it.status.value = orderFilter.status
            it.date.value = orderFilter.status
        }
        val scope = viewLifecycleOwner.lifecycleScope
        scope.launchWhenResumed { vm.statuses.collect { onLoadedStatuses(it) } }
        scope.launchWhenResumed { vm.dates.collect { onLoadedDates(it) } }

        binding.mbSubmit.setOnClickListener { submit() }
        binding.tvReset.setOnClickListener { reset() }

        load()
    }

    private fun load() {
        val accessToken = sharedPreferences.getString(AppConstant.ACCESS_TOKEN, null)
        vm.getParameters(accessToken)
    }

    private fun onLoadedStatuses(statuses: MutableList<Label>?) {
        statuses?.forEach {
            binding.cgOrderFilterStatus.addView(Chip(requireContext()).apply {
                setEnsureMinTouchTargetSize(false)
                isChecked = orderFilter.status == it.value
                text = it.label
                id = View.generateViewId()
                setOnClickListener { _ ->
                    clearFilterChips(binding.cgOrderFilterStatus)
                    orderFilter.status = it.value
                    isChecked = true
                }
            })
        }
    }

    private fun onLoadedDates(dates: MutableList<Label>?) {
        dates?.forEach {
            binding.cgOrderFilterDate.addView(Chip(requireContext()).apply {
                setEnsureMinTouchTargetSize(false)
                isChecked = orderFilter.date == it.value
                text = it.label
                id = View.generateViewId()
                setOnClickListener { _ ->
                    clearFilterChips(binding.cgOrderFilterStatus)
                    orderFilter.date = it.value
                    isChecked = true
                }
            })
        }
    }

    private fun reset() {
        sharedPreferences.edit()
            .remove(OrderConstant.FILTER_BUNDLE)
            .remove(AppConstant.FILTERED)
            .apply()
        vm.apply {
            orderFilter = OrderFilter()
            status.value = null
            date.value = null
            clearFilterChips(binding.cgOrderFilterStatus)
            clearFilterChips(binding.cgOrderFilterDate)
        }
    }

    private fun submit() {
        sharedPreferences.edit()
            .putString(OrderConstant.FILTER_BUNDLE, Gson().toJson(orderFilter))
            .putBoolean(AppConstant.FILTERED, true)
            .apply()
        setFragmentResult(
            OrderConstant.FILTER_BUNDLE,
            bundleOf(OrderConstant.FILTER_BUNDLE to true)
        )
        findNavController().navigateUp()
    }

    private fun clearFilterChips(chipGroup: ChipGroup) {
        for (id in chipGroup.checkedChipIds) {
            val c = chipGroup.findViewById<Chip>(id)
            c.isChecked = false
        }
    }
}