package multi.platform.order.app.order

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.internal.ViewUtils.dpToPx
import multi.platform.core.shared.external.constant.AppConstant
import multi.platform.core.shared.external.extension.loadImage
import multi.platform.core.shared.external.extension.toCurrency
import multi.platform.order.databinding.OrderItemBinding
import multi.platform.order.shared.domain.order.entity.Order
import multi.platform.core.shared.R as cR


class OrderAdapter(
    private val widthRatio: Double = 1.0,
    private val height: Int = 0,
) :
    RecyclerView.Adapter<OrderAdapter.ViewHolder>() {
    var onClick: ((Order) -> Unit)? = null
    var items = mutableListOf<Order>()

    class ViewHolder(
        private val binding: OrderItemBinding,
        private val onClick: ((Order) -> Unit)?
    ) :
        RecyclerView.ViewHolder(binding.root) {
        @Suppress("RestrictedApi")
        fun bind(item: Order) {
            cR.menu.menu_bottom
            val context = binding.root.context
            binding.tvOrderItemDate.text = item.date
            binding.tvOrderItemTrip.text = item.title
            binding.tvOrderItemNumber.text = item.number
            binding.tvOrderItemPrice.text = item.price.toCurrency(AppConstant.CURRENCY)
            binding.tvOrderItemStatus.apply {
                text = item.status
                when (item.status) {
                    "Pembayaran Gagal" -> {
                        setBackgroundResource(cR.drawable.bg_redlight_round8)
                        setTextColor(ContextCompat.getColor(context, cR.color.negative))
                    }

                    "Pembayaran Berhasil" -> {
                        setBackgroundResource(cR.drawable.bg_greenlight_round8)
                        setTextColor(ContextCompat.getColor(context, cR.color.primary))
                    }

                    "Menunggu Pembayaran" -> {
                        setBackgroundResource(cR.drawable.bg_mustardlight_round8)
                        setTextColor(ContextCompat.getColor(context, cR.color.info))
                    }
                }
                val dp8 = dpToPx(context, 8).toInt()
                val dp5 = dpToPx(context, 5).toInt()
                setPadding(dp8, dp5, dp8, dp5)
            }
            item.thumbnail?.let {
                binding.ivOrderItem.loadImage(it)
            }
            binding.root.setOnClickListener {
                onClick?.invoke(item)
            }
        }
    }

    @Suppress("RestrictedApi")
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            OrderItemBinding.inflate(
                LayoutInflater.from(viewGroup.context),
                viewGroup,
                false
            )
        if (widthRatio != 1.0) binding.root.layoutParams.width =
            (viewGroup.measuredWidth * widthRatio).toInt()
        if (height > 0) binding.root.layoutParams.height =
            dpToPx(binding.root.context, height).toInt()
        return ViewHolder(binding, onClick)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(items[position])
    }

    override fun getItemCount() = items.size

    @Suppress("UNUSED")
    fun clear() {
        val size = items.size
        items = mutableListOf()
        notifyItemRangeRemoved(0, size)
    }
}