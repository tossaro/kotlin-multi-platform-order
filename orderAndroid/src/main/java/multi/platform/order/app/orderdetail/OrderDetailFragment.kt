package multi.platform.order.app.orderdetail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import multi.platform.core.shared.app.common.BaseFragment
import multi.platform.core.shared.external.constant.AppConstant
import multi.platform.core.shared.external.extension.goTo
import multi.platform.core.shared.external.extension.loadImage
import multi.platform.core.shared.external.extension.showErrorSnackbar
import multi.platform.order.R
import multi.platform.order.databinding.OrderDetailFragmentBinding
import multi.platform.order.shared.app.orderdetail.OrderDetailViewModel
import multi.platform.order.shared.domain.order.entity.Order
import org.koin.androidx.viewmodel.ext.android.viewModel

class OrderDetailFragment :
    BaseFragment<OrderDetailFragmentBinding>(R.layout.order_detail_fragment) {
    private val vm: OrderDetailViewModel by viewModel()
    override fun actionBarTitle() = getString(R.string.order_detail_title)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.vm = vm.also {
            it.accessToken = sharedPreferences.getString(AppConstant.ACCESS_TOKEN, null)
        }
        val scope = viewLifecycleOwner.lifecycleScope
        scope.launchWhenResumed { vm.loadingIndicator.collect { showFullLoading(it) } }
        scope.launchWhenResumed { vm.errorMessage.collect { showErrorSnackbar(it) } }
        scope.launchWhenResumed { vm.order.collect { onLoadedOrder(it) } }
        vm.getOrder(Integer.valueOf(arguments?.getString("id") ?: "0"))
    }

    private fun onLoadedOrder(order: Order?) {
        order?.let { o ->
            binding.ivOrderDetail.loadImage(o.thumbnail.toString())
            binding.tvOrderDetailSeeDetail.setOnClickListener {
                goTo(getString(R.string.route_product_detail).replace("{id}", o.packageId))
            }
        }
    }
}