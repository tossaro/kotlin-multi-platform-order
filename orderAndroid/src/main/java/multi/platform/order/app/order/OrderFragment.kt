package multi.platform.order.app.order

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import multi.platform.core.shared.app.common.BaseFragment
import multi.platform.core.shared.external.constant.AppConstant
import multi.platform.core.shared.external.extension.dpToPx
import multi.platform.core.shared.external.extension.goTo
import multi.platform.core.shared.external.extension.showErrorSnackbar
import multi.platform.order.R
import multi.platform.order.databinding.OrderFragmentBinding
import multi.platform.order.external.constant.OrderConstant
import multi.platform.order.shared.app.order.OrderViewModel
import multi.platform.order.shared.domain.order.entity.Order
import multi.platform.order.shared.domain.order.entity.OrderFilter
import org.koin.androidx.viewmodel.ext.android.viewModel
import multi.platform.core.shared.R as cR


class OrderFragment : BaseFragment<OrderFragmentBinding>(R.layout.order_fragment), MenuProvider {
    private val vm: OrderViewModel by viewModel()
    private lateinit var orderAdapter: OrderAdapter
    var currentScrollPosition = 0
    private var page = 1

    override fun actionBarTitle() = getString(cR.string.menu_order)
    override fun showBottomNavBar() = true
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onCreateMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_order, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.filter -> goTo(getString(R.string.route_order_filter_sheet))
        }
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(OrderConstant.FILTER_BUNDLE) { _, b ->
            if (b.getBoolean(OrderConstant.FILTER_BUNDLE)) {
                setActionBarSearchIcon()
                val filterStr = sharedPreferences.getString(OrderConstant.FILTER_BUNDLE, null)
                filterStr?.let {
                    vm.orderFilter.value = Gson().fromJson(it, OrderFilter::class.java)
                }
                orderAdapter.clear()
                page = 1
                load()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.vm = vm.also {
            it.accessToken = sharedPreferences.getString(AppConstant.ACCESS_TOKEN, null)
        }
        val scope = viewLifecycleOwner.lifecycleScope
        scope.launchWhenResumed { vm.loadingIndicator.collect { showFullLoading(it) } }
        scope.launchWhenResumed { vm.errorMessage.collect { showErrorSnackbar(it) } }
        scope.launchWhenResumed { vm.orders.collect { onLoadedOrders(it) } }

        sharedPreferences.edit().remove(AppConstant.FILTERED).remove(OrderConstant.FILTER_BUNDLE)
            .apply()

        orderAdapter = OrderAdapter()
        orderAdapter.onClick = {
            goTo(getString(R.string.route_order_detail).replace("{id}", it.id.toString()))
        }
        binding.rvOrder.adapter = orderAdapter
        binding.rvOrder.isNestedScrollingEnabled = false
        binding.rvOrder.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State
            ) {
                outRect.bottom = dpToPx(2f)
                outRect.left = dpToPx(6f)
                outRect.right = dpToPx(6f)
            }
        })
        binding.rvOrder.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (!recyclerView.canScrollVertically(1)) {
                    page++
                    load()
                }
                currentScrollPosition += dy
                binding.srlOrder.isEnabled = currentScrollPosition == 0
            }
        })
        binding.rvOrder.isEnabled = false
        binding.srlOrder.setOnRefreshListener {
            page = 1
            clear()
            load()
        }
        binding.mbOrderSeePackages.setOnClickListener {
            goTo(getString(R.string.route_product_recommendation))
        }
        load()
    }

    private fun load() {
        vm.getOrders(page)
    }

    private fun clear() {
        orderAdapter.clear()
        vm.orders.value = null
    }

    private fun onLoadedOrders(orders: MutableList<Order>?) {
        orders?.takeIf { it.size > 0 }?.let { os ->
            orderAdapter.items.let { o ->
                val sizeBefore = o.size
                o.addAll(os)
                val sizeAfter = o.size - 1
                orderAdapter.notifyItemRangeChanged(sizeBefore, sizeAfter)
            }
        }
        binding.srlOrder.isRefreshing = false
    }
}