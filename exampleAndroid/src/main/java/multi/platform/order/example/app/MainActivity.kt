package multi.platform.order.example.app

import multi.platform.core.shared.app.common.BaseActivity
import multi.platform.order.example.R
import multi.platform.auth.R as aR
import multi.platform.order.R as tR

class MainActivity : BaseActivity() {
    override fun appVersion() = getString(R.string.app_version)
    override fun navGraph() = R.navigation.main_nav_graph
    override fun topLevelDestinations(): Set<Int> {
        val list = HashSet<Int>()
        list.add(aR.id.signInFragment)
        list.add(aR.id.registerFragment)
        list.add(tR.id.orderFragment)
        list.add(R.id.profileFragment)
        return list
    }
}