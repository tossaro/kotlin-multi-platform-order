package multi.platform.order.example.app.home

import multi.platform.core.shared.app.common.BaseFragment
import multi.platform.order.example.R
import multi.platform.order.example.databinding.HomeFragmentBinding

class HomeFragment : BaseFragment<HomeFragmentBinding>(R.layout.home_fragment) {
    override fun actionBarTitle() = getString(R.string.title)
    override fun showBottomNavBar() = true
}